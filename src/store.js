import Vue from 'vue'
import Vuex from 'vuex'

import Css from '@/store/css'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    Css
  }
})
