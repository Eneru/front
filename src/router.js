import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/attendus',
      name: 'expected',
      component: () => import('@/views/Expected/Home')
    },
    {
      path: '/modele-client-server/client-server',
      name: 'modelClientServer-clientServer',
      component: () => import('@/views/ModelClientServer/ClientServer/Home'),
      children: [
        {
          path: '/modele-client-server/client-server/client',
          name: 'modelClientServer-clientServer-client',
          component: () => import('@/views/ModelClientServer/ClientServer/Client')
        },
        {
          path: '/modele-client-server/client-server/internet',
          name: 'modelClientServer-clientServer-internet',
          component: () => import('@/views/ModelClientServer/ClientServer/Internet')
        },
        {
          path: '/modele-client-server/client-server/server',
          name: 'modelClientServer-clientServer-server',
          component: () => import('@/views/ModelClientServer/ClientServer/Server')
        }
      ]
    },
    {
      path: '/modele-client-server/requete-reponse/exemples',
      name: 'modelClientServer-clientServer-requestResponse',
      component: () => import('@/views/ModelClientServer/RequestResponse/Home'),
      children: [
        {
          path: '/modele-client-server/requete-reponse/requete',
          name: 'modelClientServer-clientServer-requestResponse-request',
          component: () => import('@/views/ModelClientServer/RequestResponse/Request')
        },
        {
          path: '/modele-client-server/requete-reponse/requete/exemples',
          name: 'modelClientServer-clientServer-requestResponse-requestExamples',
          component: () => import('@/views/ModelClientServer/RequestResponse/RequestExamples'),
          children: [
            {
              path: '/modele-client-server/requete-reponse/requete/exemples/post',
              name: 'modelClientServer-clientServer-requestResponse-requestExamples-post',
              component: () => import('@/components/ModelClientServer/ClientServer/RequestExamplePost')
            },
            {
              path: '/modele-client-server/requete-reponse/requete/exemples/get-all',
              name: 'modelClientServer-clientServer-requestResponse-requestExamples-getAll',
              component: () => import('@/components/ModelClientServer/ClientServer/RequestExampleGetAll')
            },
            {
              path: '/modele-client-server/requete-reponse/requete/exemples/get/:id',
              name: 'modelClientServer-clientServer-requestResponse-requestExamples-get',
              component: () => import('@/components/ModelClientServer/ClientServer/RequestExampleGetOne')
            },
            {
              path: '/modele-client-server/requete-reponse/requete/exemples/patch-put',
              name: 'modelClientServer-clientServer-requestResponse-requestExamples-patchPut',
              component: () => import('@/components/ModelClientServer/ClientServer/RequestExamplePatch')
            },
            {
              path: '/modele-client-server/requete-reponse/requete/exemples/delete',
              name: 'modelClientServer-clientServer-requestResponse-requestExamples-delete',
              component: () => import('@/components/ModelClientServer/ClientServer/RequestExampleDelete')
            }
          ]
        },
        {
          path: '/modele-client-server/requete-reponse/reponse',
          name: 'modelClientServer-clientServer-requestResponse-response',
          component: () => import('@/views/ModelClientServer/RequestResponse/Response')
        },
        {
          path: '/modele-client-server/requete-reponse/reponse/exemples',
          name: 'modelClientServer-clientServer-requestResponse-responseExamples',
          component: () => import('@/views/ModelClientServer/RequestResponse/ResponseExamples')
        }
      ]
    },
    {
      path: '/modele-client-server/exemples-applications',
      name: 'modelClientServer-clientServer-applicationExamples',
      component: () => import('@/views/ModelClientServer/ApplicationExamples/Home'),
      children: [
        {
          path: '/modele-client-server/exemples-applications/simple',
          name: 'modelClientServer-clientServer-applicationExamples-simple',
          component: () => import('@/components/ModelClientServer/ApplicationsExamples/Simple')
        },
        {
          path: '/modele-client-server/exemples-applications/personnalisee',
          name: 'modelClientServer-clientServer-applicationExamples-personalized',
          component: () => import('@/components/ModelClientServer/ApplicationsExamples/Personalized')
        },
        {
          path: '/modele-client-server/exemples-applications/animee',
          name: 'modelClientServer-clientServer-applicationExamples-animated',
          component: () => import('@/components/ModelClientServer/ApplicationsExamples/Animated')
        },
        {
          path: '/modele-client-server/exemples-applications/ajax',
          name: 'modelClientServer-clientServer-applicationExamples-ajax',
          component: () => import('@/components/ModelClientServer/ApplicationsExamples/Ajax')
        },
        {
          path: '/modele-client-server/exemples-applications/application-front-lourde',
          name: 'modelClientServer-clientServer-applicationExamples-applicationFront',
          component: () => import('@/components/ModelClientServer/ApplicationsExamples/AppFront')
        }
      ]
    },
    {
      path: '/html/generalites',
      name: 'html-general',
      component: () => import('@/views/HTML/General/Home'),
      children: [
        {
          path: '/html/generalites/page',
          name: 'html-general-page',
          component: () => import('@/views/HTML/General/Page')
        },
        {
          path: '/html/generalites/head',
          name: 'html-general-head',
          component: () => import('@/views/HTML/General/Head')
        },
        {
          path: '/html/generalites/balise',
          name: 'html-general-tags',
          component: () => import('@/views/HTML/General/Tags')
        },
        {
          path: '/html/generalites/attributs',
          name: 'html-general-attributes',
          component: () => import('@/views/HTML/General/Attributes')
        }
      ]
    },
    {
      path: '/html/balises',
      name: 'html-tags',
      component: () => import('@/views/HTML/Tags/Home'),
      children: [
        {
          path: '/html/balises/texte',
          name: 'html-tags-text',
          component: () => import('@/views/HTML/Tags/Text')
        },
        {
          path: '/html/balises/lien',
          name: 'html-tags-link',
          component: () => import('@/views/HTML/Tags/Link')
        },
        {
          path: '/html/balises/liste',
          name: 'html-tags-list',
          component: () => import('@/views/HTML/Tags/List')
        },
        {
          path: '/html/balises/image',
          name: 'html-tags-image',
          component: () => import('@/views/HTML/Tags/Image')
        },
        {
          path: '/html/balises/formulaire',
          name: 'html-tags-form',
          component: () => import('@/views/HTML/Tags/Form')
        },
        {
          path: '/html/balises/tableau',
          name: 'html-tags-table',
          component: () => import('@/views/HTML/Tags/Table')
        },
        {
          path: '/html/balises/audio',
          name: 'html-tags-audio',
          component: () => import('@/views/HTML/Tags/Audio')
        }
      ]
    },
    {
      path: '/html/structure',
      name: 'html-structure',
      component: () => import('@/views/HTML/Structure/Home'),
      children: [
        {
          path: '/html/structure/structure',
          name: 'html-structure-structure',
          component: () => import('@/views/HTML/Structure/Structure')
        },
        {
          path: '/html/structure/decoupage',
          name: 'html-structure-cutting',
          component: () => import('@/views/HTML/Structure/Cutting')
        },
        {
          path: '/html/structure/accessibilite',
          name: 'html-structure-accessibility',
          component: () => import('@/views/HTML/Structure/Accessibility')
        }
      ]
    },
    {
      path: '/cours-css/generalites',
      name: 'css-general',
      component: () => import('@/views/CSS/General/Home'),
      children: [
        {
          path: '/cours-css/generalites/generalites',
          name: 'css-general-general',
          component: () => import('@/views/CSS/General/General')
        },
        {
          path: '/cours-css/generalites/selection',
          name: 'css-general-selector',
          component: () => import('@/views/CSS/General/Selector')
        },
        {
          path: '/cours-css/generalites/unites',
          name: 'css-general-unity',
          component: () => import('@/views/CSS/General/Unit')
        },
        {
          path: '/cours-css/generalites/divers',
          name: 'css-general-divers',
          component: () => import('@/views/CSS/General/Divers')
        }
      ]
    },
    {
      path: '/cours-css/technologies',
      name: 'css-technology',
      component: () => import('@/views/CSS/Technology/Home'),
      children: [
        {
          path: '/cours-css/technologies/bootstrap',
          name: 'css-technology-bootstrap',
          component: () => import('@/views/CSS/Technology/Bootstrap')
        },
        {
          path: '/cours-css/technologies/grid-flex',
          name: 'css-technology-gridFlex',
          component: () => import('@/views/CSS/Technology/GridFlex')
        },
        {
          path: '/cours-css/technologies/media-queries',
          name: 'css-technology-mediaQueries',
          component: () => import('@/views/CSS/Technology/MediaQueries')
        },
        {
          path: '/cours-css/technologies/sass',
          name: 'css-technology-sass',
          component: () => import('@/views/CSS/Technology/Sass')
        },
        {
          path: '/cours-css/technologies/animation',
          name: 'css-technology-animation',
          component: () => import('@/views/CSS/Technology/Animation')
        }
      ]
    },
    {
      path: '/cours-css/test',
      name: 'css-test',
      component: () => import('@/views/CSS/Test/Test')
    },
    {
      path: '/cours-js/javascript',
      name: 'js-javascript',
      component: () => import('@/views/JS/Langage/Home'),
      children: [
        {
          path: '/cours-js/javascript/le-langage',
          name: 'js-javascript-langage',
          component: () => import('@/views/JS/Langage/Language')
        },
        {
          path: '/cours-js/javascript/ECMAScript',
          name: 'js-javascript-ecma',
          component: () => import('@/views/JS/Langage/ECMA')
        },
        {
          path: '/cours-js/javascript/synchronicite',
          name: 'js-javascript-sync',
          component: () => import('@/views/JS/Langage/Sync')
        }
      ]
    },
    {
      path: '/cours-js/web',
      name: 'js-web',
      component: () => import('@/views/JS/Web/Home'),
      children: [
        {
          path: '/cours-js/web/DOM',
          name: 'js-web-dom',
          component: () => import('@/views/JS/Web/DOM')
        },
        {
          path: '/cours-js/web/evenements',
          name: 'js-web-event',
          component: () => import('@/views/JS/Web/Event')
        },
        {
          path: '/cours-js/web/AJAX',
          name: 'js-web-ajax',
          component: () => import('@/views/JS/Web/AJAX')
        },
        {
          path: '/cours-js/web/regex',
          name: 'js-web-regex',
          component: () => import('@/views/JS/Web/Regex')
        }
      ]
    },
    {
      path: '/cours-js/technologies',
      name: 'js-technology',
      component: () => import('@/views/JS/Technology/Home'),
      children: [
        {
          path: '/cours-js/technologies/JQuery',
          name: 'js-technology-jquery',
          component: () => import('@/views/JS/Technology/JQuery')
        },
        {
          path: '/cours-js/technologies/Framework',
          name: 'js-technology-framework',
          component: () => import('@/views/JS/Technology/Framework')
        },
        {
          path: '/cours-js/technologies/plus-loin',
          name: 'js-technology-plus',
          component: () => import('@/views/JS/Technology/Plus')
        }
      ]
    },
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Home')
    }
  ]
})
