const state = {
  css: {
    background: 'rgb(250, 250, 250)',
    borderTop: '',
    borderBottom: '',
    borderRight: '',
    borderLeft: '',
    borderRadius: '0px',
    boxShadow: 'none',
    boxSizing: 'border-box',
    color: '#000000',
    cursor: 'text',
    display: 'block',
    fontFamily: 'Raleway',
    fontSize: '16px',
    fontStyle: 'normal',
    fontVariant: 'normal',
    fontWeight: 'normal',
    height: 'auto',
    letterSpacing: 'normal',
    lineHeight: '24px',
    marginTop: '0px',
    marginBottom: '0px',
    marginLeft: '0px',
    marginRight: '0px',
    opacity: 1,
    overflowY: 'auto',
    paddingTop: '0px',
    paddingBottom: '0px',
    paddingLeft: '0px',
    paddingRight: '0px',
    position: 'sticky',
    textDecoration: 'none',
    textAlign: 'left',
    textIndent: '0px',
    textShadow: 'none',
    textTransform: 'none',
    top: '10px',
    width: 'auto'
  },
  placementActive: false,
  placementMobilParagraph: {
    position: 'static',
    top: 'auto',
    bottom: 'auto',
    left: 'auto',
    right: 'auto'
  }
}

const getters = {
  css: state => state.css,
  placementActive: state => state.placementActive,
  placementMobilParagraph: state => state.placementMobilParagraph
}

const actions = {
  setValue: ({ commit }, datas) => {
    datas.cssGroup = 'css'
    commit('SET_VALUE', datas)
  },
  setPlacementValue: ({ commit }, datas) => {
    datas.cssGroup = 'placementMobilParagraph'
    commit('SET_VALUE', datas)
  },
  switchActive: ({ commit }, isActive) => {
    commit('SWITCH_ACTIVE', isActive)
  }
}

const mutations = {
  SET_VALUE: (state, { cssGroup, valueName, val }) => {
    state[cssGroup][valueName] = val
  },
  SWITCH_ACTIVE: (state, isActive) => {
    state.placementActive = isActive
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
