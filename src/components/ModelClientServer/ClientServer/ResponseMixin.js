export default {
  props: {
    color: {
      required: true,
      type: String
    }
  },
  data () {
    return {
      statusCode: null
    }
  },
  computed: {
    getImage: function () {
      return {
        url: `https://http.cat/${this.statusCode}`,
        alt: `Cat with status code ${this.statusCode}`
      }
    }
  },
  methods: {
    onChangeComponent (statusCode) {
      this.statusCode = parseInt(statusCode)
    }
  }
}
